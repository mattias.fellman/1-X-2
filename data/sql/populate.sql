

-- FORMATTING STARTED FOR UEFA 2020


-- Set up event, utc time for last bet!
INSERT INTO betevents (external_id, name, year, group_matches, betting_starts, betting_ends) 
VALUES ('467', 'UEFA 2020', '2020', '36', '2020-02-01 09:00', '2020-06-12 15:00');
SET @event_id = LAST_INSERT_ID();

-- Set up betting objects 42 matches
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165069', 'RUS-KSA', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165084', 'EGY-URU', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165083', 'MAR-IRN', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165076', 'POR-ESP', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165072', 'FRA-AUS', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165073', 'ARG-ISL', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165071', 'PER-DEN', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165074', 'CRO-NGA', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165075', 'CRC-SRB', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165082', 'GER-MEX', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165070', 'BRA-SUI', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165081', 'SWE-KOR', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165077', 'BEL-PAN', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165078', 'TUN-ENG', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165080', 'COL-JPN', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165079', 'POL-SEN', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165100', 'RUS-EGY', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165087', 'POR-MAR', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165086', 'URU-KSA', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165085', 'IRN-ESP', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165099', 'DEN-AUS', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165096', 'FRA-PER', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165094', 'ARG-CRO', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165092', 'BRA-CRC', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165098', 'NGA-ISL', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165091', 'SRB-SUI', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165088', 'BEL-TUN', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165089', 'KOR-MEX', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165090', 'GER-SWE', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165093', 'ENG-PAN', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165095', 'JPN-SEN', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165097', 'POL-COL', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165111', 'KSA-EGY', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165101', 'URU-RUS', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165112', 'IRN-POR', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165109', 'ESP-MAR', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165113', 'DEN-FRA', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165107', 'AUS-PER', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165115', 'NGA-ARG', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165114', 'ISL-CRO', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165102', 'MEX-SWE', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165106', 'KOR-GER', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165116', 'SRB-BRA', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165108', 'SUI-CRC', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165104', 'JPN-POL', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165103', 'SEN-COL', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165110', 'PAN-TUN', 1);
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '165105', 'ENG-BEL', 1);

INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'RoundOf16', '1');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Quarterfinal', '2');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Quarterfinal', '2');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Quarterfinal', '2');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Quarterfinal', '2');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Quarterfinal', '2');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Quarterfinal', '2');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Quarterfinal', '2');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Quarterfinal', '2');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Semifinal', '3');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Semifinal', '3');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Semifinal', '3');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Semifinal', '3');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Bronze', '3');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Silver', '5');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Gold', '7');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Scorer', '3');
INSERT INTO betobjects (event_id, external_id, name, value) VALUES (@event_id, '0', 'Referee', '2');
