CREATE DATABASE IF NOT EXISTS `1-x-2` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_swedish_ci */;
USE `1-x-2`;

DROP TABLE IF EXISTS `1-x-2`.`bets`;
DROP TABLE IF EXISTS `1-x-2`.`betslips`;
DROP TABLE IF EXISTS `1-x-2`.`betobjects`;
DROP TABLE IF EXISTS `1-x-2`.`betevents`;
DROP TABLE IF EXISTS `1-x-2`.`sessions`;
DROP TABLE IF EXISTS `1-x-2`.`users`;

CREATE TABLE IF NOT EXISTS `1-x-2`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(64) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `role` INT NOT NULL DEFAULT 1,
  `active` BOOLEAN NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uuid_UNIQUE` (`uuid` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC));

CREATE TABLE IF NOT EXISTS `1-x-2`.`sessions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(64) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `role` INT NOT NULL,
  `user_id` INT NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uuid_UNIQUE` (`uuid` ASC),
  INDEX `fk_sessions_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_sessions_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `1-x-2`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE IF NOT EXISTS `1-x-2`.`betevents` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `external_id` INT DEFAULT '0',
  `name` VARCHAR(255) NOT NULL,
  `year` INT DEFAULT '2018' NOT NULL,
  `group_matches` INT DEFAULT '48' NOT NULL,
  `betting_starts` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `betting_ends` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC),
  INDEX `betevents1_idx` (`year` ASC));

CREATE TABLE IF NOT EXISTS `1-x-2`.`betobjects` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `event_id` INT NOT NULL,
  `external_id` INT DEFAULT '0',
  `name` VARCHAR(255) NOT NULL,
  `type` VARCHAR(255) NOT NULL,
  `value` INT DEFAULT '1' NOT NULL,
  `winner` VARCHAR(255) DEFAULT '' NOT NULL,
  `winner_info` VARCHAR(255) DEFAULT '' NOT NULL,
  `home_score` INT,
  `away_score` INT,
  `correct_bets` INT DEFAULT '0' NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_betevents_objects1_idx` (`event_id` ASC),
  CONSTRAINT `fk_betevents_objects1`
      FOREIGN KEY (`event_id`)
      REFERENCES `1-x-2`.`betevents` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION);

CREATE TABLE IF NOT EXISTS `1-x-2`.`betslips` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `event_id` INT NOT NULL,
  `pseudonym` VARCHAR(255) NOT NULL,
  `correct_answers` INT DEFAULT '0' NOT NULL,
  `points` INT DEFAULT '0' NOT NULL,
  `approved` BOOLEAN DEFAULT '0' NOT NULL,
  `discarded` BOOLEAN DEFAULT '0' NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_betslips_users1_idx` (`user_id` ASC),
  INDEX `fk_betslips_betevents1_idx` (`event_id` ASC),
  CONSTRAINT `fk_betslips_users1`
      FOREIGN KEY (`user_id`)
      REFERENCES `1-x-2`.`users` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  CONSTRAINT `fk_betslips_betevents1`
      FOREIGN KEY (`event_id`)
      REFERENCES `1-x-2`.`betevents` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION);

CREATE TABLE IF NOT EXISTS `1-x-2`.`bets` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `slip_id` INT NOT NULL,
  `object_id` INT NOT NULL,
  `winner` VARCHAR(255) DEFAULT '' NOT NULL,
  `winner_info` VARCHAR(255) DEFAULT '' NOT NULL,
  `home_score` INT,
  `away_score` INT,
  `correct` BOOLEAN DEFAULT '0' NOT NULL,
  `incorrect` BOOLEAN DEFAULT '0' NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_bets_betslips1_idx` (`slip_id` ASC),
  INDEX `fk_bets_betobjects1_idx` (`object_id` ASC),
  CONSTRAINT `fk_bets_betslips1`
      FOREIGN KEY (`slip_id`)
      REFERENCES `1-x-2`.`betslips` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  CONSTRAINT `fk_bets_betobjects1`
      FOREIGN KEY (`object_id`)
      REFERENCES `1-x-2`.`betobjects` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION);

