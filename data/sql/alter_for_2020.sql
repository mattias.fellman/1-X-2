# Taking note of the changes that should be done in the db
# Compare with origininal setup 

ALTER TABLE betobjects 
ADD `object_type` VARCHAR(64) NOT NULL DEFAULT '' AFTER external_id,
ADD `takes_place` TIMESTAMP DEFAULT CURRENT_TIMESTAMP AFTER value;

ALTER TABLE users
DROP COLUMN auth_level,
ADD `role` INT NOT NULL DEFAULT 1 AFTER password;

ALTER TABLE sessions
DROP COLUMN auth_level,
ADD `role` INT NOT NULL DEFAULT 1 AFTER email;