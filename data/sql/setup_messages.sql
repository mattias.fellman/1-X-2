DROP TABLE IF EXISTS `blog`;

CREATE TABLE IF NOT EXISTS `blog` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `category` VARCHAR(20) DEFAULT 'Fun' NOT NULL,
  `author` VARCHAR(255) NOT NULL,
  `message` TEXT NOT NULL,
  `visible` BOOLEAN DEFAULT '1' NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  );
