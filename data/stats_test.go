package data

import (
	"database/sql"
	"testing"
)

func Test_KnockoutStats(t *testing.T) {
	ev, err := KnockoutStats()
	if err != nil {
		t.Error(err, "Can't get knockout statistics.")
	}
}
