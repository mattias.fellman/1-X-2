package data

// test data
var users = []User{
	{
		Name:     "Peter Jones",
		Email:    "peter@gmail.com",
		Password: "peter_pass",
		Role:     1,
		Active:   true,
	},
	{
		Name:     "John Smith",
		Email:    "john@gmail.com",
		Password: "john_pass",
		Role:     2,
		Active:   true,
	},
}

// test data
var posts = []Blogpost{
	{
		Id:       1,
		Category: "Fun",
		Author:   "Author August",
		Message:  "August message",
		Visible:  true,
	},
	{
		Id:       2,
		Category: "Fact",
		Author:   "Author Bert",
		Message:  "Bert title",
		Visible:  true,
	},
}

func setup() {
	SessionDeleteAll()
	UserDeleteAll()
	BlogpostDeleteAll()
}
