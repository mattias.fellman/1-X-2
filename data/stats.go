package data

import ()

type Stat struct {
	ObjectName string
	PlacedBet  string
	Num        int
}

// Get statistics for all except group matches
func KnockoutStats(event_id int) (stats []Stat, err error) {
	rows, err := Db.Query(`SELECT o.name, b.result, COUNT(*) AS num
		FROM bets b
		LEFT JOIN betobjects o ON o.id = b.object_id
		LEFT JOIN betslips s ON s.id = b.slip_id
		WHERE o.name NOT LIKE '%-%' AND s.approved = 1 AND o.event_id = ?
		GROUP BY o.name, b.result
		ORDER BY o.name, num DESC`, event_id)
	if err != nil {
		return
	}
	for rows.Next() {
		stat := Stat{}
		if err = rows.Scan(&stat.ObjectName, &stat.PlacedBet, &stat.Num); err != nil {
			return
		}
		stats = append(stats, stat)
	}
	rows.Close()
	return
}

// Count users
func NumUsers() (count int, err error) {
	rows, err := Db.Query(`
		SELECT count(*) 
		FROM users 
		WHERE active = 1
		`)
	if err != nil {
		return
	}
	for rows.Next() {
		if err = rows.Scan(&count); err != nil {
			return
		}
	}
	rows.Close()
	return
}

// Count betslips for event per status
func NumBetSlips(approved int, discarded int, eventId int) (count int, err error) {
	rows, err := Db.Query(`
		SELECT count(*) 
		FROM betslips 
		WHERE approved = ? AND discarded = ? AND event_id = ?
		`, approved, discarded, eventId)
	if err != nil {
		return
	}
	for rows.Next() {
		if err = rows.Scan(&count); err != nil {
			return
		}
	}
	rows.Close()
	return
}
