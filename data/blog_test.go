package data

import (
	"testing"
)

func Test_BlogpostCreate(t *testing.T) {
	setup()
	if err := posts[0].Create(); err != nil {
		t.Error(err, "Cannot create blogpost.")
	}
}

func Test_BlogpostDelete(t *testing.T) {
	setup()
	if err := posts[0].Create(); err != nil {
		t.Error(err, "Cannot create blogpost.")
	}
	if err := posts[0].Delete(); err != nil {
		t.Error(err, "- Cannot delete blogpost")
	}
}

func Test_BlogpostUpdate(t *testing.T) {
	setup()
	if err := posts[0].Create(); err != nil {
		t.Error(err, "Cannot create blogpost.")
	}
	posts[0].Title = "Random title"
	if err := posts[0].Update(); err != nil {
		t.Error(err, "- Cannot update blogpost")
	}
}

func Test_Blogposts(t *testing.T) {
	setup()
	for _, post := range posts {
		if err := post.Create(); err != nil {
			t.Error(err, "Cannot create blogpost.")
		}
	}
	m, err := posts()
	if err != nil {
		t.Error(err, "Cannot retrieve posts.")
	}
	if len(m) != 2 {
		t.Error(err, "Wrong number of posts retrieved")
	}
}
