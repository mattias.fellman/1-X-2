package data

import (
	"time"
)

type Event struct {
	Id            int
	ExternalId    int
	Name          string
	Year          int
	GroupMatches  int
	BettingStarts time.Time
	BettingEnds   time.Time
	CreatedAt     time.Time
	UpdatedAt     time.Time
}

// Get a single event given the year
func EventByYear(year int) (event Event, err error) {
	event = Event{}
	err = Db.QueryRow("SELECT id, external_id, name, year, group_matches, betting_starts, betting_ends, created_at, updated_at FROM betevents WHERE year = ?", year).
		Scan(&event.Id, &event.ExternalId, &event.Name, &event.Year, &event.GroupMatches, &event.BettingStarts, &event.BettingEnds, &event.CreatedAt, &event.UpdatedAt)
	return
}

// Reset the event; results, correct/incorrect info and points
func ResetEvent(eventId int) (err error) {
	statement1 := "UPDATE betobjects SET result = '', correct_bets = 0 WHERE event_id = ?"
	stmt1, err := Db.Prepare(statement1)
	if err != nil {
		return
	}
	defer stmt1.Close()
	_, err = stmt1.Exec(eventId)

	statement2 := `UPDATE bets SET correct = 0, incorrect = 0 
				   WHERE object_id IN 
				   (
				   	SELECT id FROM betobjects 
				   	WHERE event_id = ?
				   )`
	stmt2, err := Db.Prepare(statement2)
	if err != nil {
		return
	}
	defer stmt2.Close()
	_, err = stmt2.Exec(eventId)

	statement3 := "UPDATE betslips SET correct_answers = 0, points = 0 WHERE event_id = ?"
	stmt3, err := Db.Prepare(statement3)
	if err != nil {
		return
	}
	defer stmt3.Close()
	_, err = stmt3.Exec(eventId)
	return
}
