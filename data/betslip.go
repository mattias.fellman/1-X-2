package data

import (
	"time"
)

type BetSlip struct {
	Id             int64
	UserId         int
	EventId        int
	Pseudonym      string
	CorrectAnswers int
	Points         int
	Approved       bool
	Discarded      bool
	CreatedAt      time.Time
	UpdatedAt      time.Time
}

type RankedBetSlip struct {
	Id             int64
	Pseudonym      string
	CorrectAnswers int
	Points         int
	Rank           int
	Approved       bool
	Discarded      bool
}

type BetSlipOwner struct {
	Id         int64
	Pseudonym  string
	CreatedAt  time.Time
	Owner      string
	OwnerEmail string
}

type BetJoined struct {
	Id         int64
	ObjectId   int
	Winner     string
	WinnerInfo string
	HomeScore  int
	AwayScore  int
	Correct    bool
	Incorrect  bool
	ObjectName string
	Value      int
}

// Get all betslips given a user
func (user *User) BetSlipsByUser() (slips []RankedBetSlip, err error) {
	rows, err := Db.Query(`
		SELECT 
			s.id, 
			s.pseudonym, 
			s.correct_answers, 
			s.points, 
			IF (s.approved=1, (SELECT COUNT(*)+1 FROM betslips WHERE approved = 1 AND discarded = 0 AND points > s.points), 0) as rank,
			s.approved,
			s.discarded
		FROM betslips s 
		WHERE s.discarded = 0 AND s.user_id = ?`, user.Id)
	if err != nil {
		return
	}
	for rows.Next() {
		slip := RankedBetSlip{}
		if err = rows.Scan(&slip.Id, &slip.Pseudonym, &slip.CorrectAnswers, &slip.Points, &slip.Rank, &slip.Approved, &slip.Discarded); err != nil {
			return
		}
		slips = append(slips, slip)
	}
	rows.Close()
	return
}

// Get betslips not approved or discarded
func BetSlipsToApprove(eventId int) (slips []BetSlipOwner, err error) {
	rows, err := Db.Query(`
		SELECT s.id, s.pseudonym, s.created_at, u.name, u.email 
		FROM betslips s
		LEFT JOIN users u ON u.id = s.user_id
		WHERE s.approved = 0 AND s.discarded = 0 AND event_id = ?
		ORDER BY s.pseudonym, s.created_at`, eventId)
	if err != nil {
		return
	}
	for rows.Next() {
		slip := BetSlipOwner{}
		if err = rows.Scan(&slip.Id, &slip.Pseudonym, &slip.CreatedAt, &slip.Owner, &slip.OwnerEmail); err != nil {
			return
		}
		slips = append(slips, slip)
	}
	rows.Close()
	return
}

// Approve slip
func (slip *BetSlip) Approve() (err error) {
	statement := "UPDATE betslips SET approved = 1, updated_at = NOW() WHERE id = ?"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(slip.Id)
	return
}

// Discard slip
func (slip *BetSlip) Discard() (err error) {
	statement := "UPDATE betslips SET discarded = 1, updated_at = NOW() WHERE id = ?"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(slip.Id)
	return
}

// Get a single betslip given the id
func BetSlipById(id int) (slip BetSlip, err error) {
	slip = BetSlip{}
	err = Db.QueryRow(`
		SELECT id, user_id, event_id, pseudonym, correct_answers, points, approved, discarded, created_at, updated_at 
		FROM betslips 
		WHERE id = ?`, id).
		Scan(&slip.Id, &slip.UserId, &slip.EventId, &slip.Pseudonym, &slip.CorrectAnswers, &slip.Points, &slip.Approved, &slip.Discarded, &slip.CreatedAt, &slip.UpdatedAt)
	return
}

// Get bets given the slip id
func BetsBySlipId(id int64) (bets []BetJoined, err error) {
	rows, err := Db.Query(`SELECT b.id, b.object_id, b.winner, b.winner_info, b.home_score, b.away_score, b.correct, b.incorrect, o.name, o.value
		FROM bets b
		JOIN betobjects o ON o.id = b.object_id
		WHERE b.slip_id = ? 
		ORDER BY b.object_id`, id)
	if err != nil {
		return
	}
	for rows.Next() {
		bet := BetJoined{}
		if err = rows.Scan(&bet.Id, &bet.ObjectId, &bet.Winner, &bet.WinnerInfo, &bet.HomeScore, &bet.AwayScore, &bet.Correct, &bet.Incorrect, &bet.ObjectName, &bet.Value); err != nil {
			return
		}
		bets = append(bets, bet)
	}
	rows.Close()
	return
}

// Check pseudonym
func ReservedPseudonym(pseudonym string, eventId int) (reserved bool, err error) {
	reserved = true
	var count int

	rows, err := Db.Query(`
		SELECT count(*) 
		FROM betslips 
		WHERE pseudonym = ? AND event_id = ?
		`, pseudonym, eventId)
	if err != nil {
		return
	}
	for rows.Next() {
		if err = rows.Scan(&count); err != nil {
			return
		}
	}
	rows.Close()
	if count == 0 {
		reserved = false
	}
	return
}

// Get all approved BetSlips in ranking order
func Standings(eventId int) (slips []BetSlip, err error) {
	rows, err := Db.Query(`
		SELECT id, user_id, event_id, pseudonym, correct_answers, points, approved, discarded, created_at, updated_at 
		FROM betslips 
		WHERE approved = 1 AND discarded = 0 AND event_id = ? 
		ORDER BY points DESC, correct_answers, pseudonym
		`, eventId)
	if err != nil {
		return
	}
	for rows.Next() {
		slip := BetSlip{}
		if err = rows.Scan(&slip.Id, &slip.UserId, &slip.EventId, &slip.Pseudonym, &slip.CorrectAnswers, &slip.Points, &slip.Approved, &slip.Discarded, &slip.CreatedAt, &slip.UpdatedAt); err != nil {
			return
		}
		slips = append(slips, slip)
	}
	rows.Close()
	return
}

// Create a new betslip
func (slip *BetSlip) Create() (lastId int64, err error) {
	statement := "INSERT INTO betslips (user_id, event_id, pseudonym, correct_answers, points, approved, discarded, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()

	res, err := stmt.Exec(slip.UserId, slip.EventId, slip.Pseudonym, 0, 0, 0, 0, time.Now(), time.Now())
	if err != nil {
		return
	}

	lastId, err = res.LastInsertId()
	return
}
