package data

import (
	"time"
)

type Blogpost struct {
	Id        int
	Category  string
	Author    string
	Message   string
	Visible   bool
	CreatedAt time.Time
	UpdatedAt time.Time
}

// Create a new message, save message info into the database
func (post *Blogpost) Create() (err error) {
	statement := "INSERT INTO blog (category, author, message, visible, created_at, updated_at) values (?, ?, ?, ?, ?, ?)"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()

	_, err = stmt.Exec(post.Category, post.Author, post.Message, 1, time.Now(), time.Now())
	if err != nil {
		return
	}

	return
}

// Delete post from database
func (post *Blogpost) Delete() (err error) {
	statement := "DELETE FROM blog WHERE id = ?"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(post.Id)
	return
}

// Hide / unpublish blogpost
func (post *Blogpost) Hide() (err error) {
	statement := "UPDATE blog SET visible = 0 WHERE id = ?"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(post.Id)
	return
}

// Update post information in the database
func (post *Blogpost) Update() (err error) {
	statement := "UPDATE blog SET category = ?, author = ?, message = ?, visible = ? WHERE id = ?"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(post.Category, post.Author, post.Message, post.Visible, post.Id)
	return
}

// Delete all posts from the database
func BlogpostDeleteAll() (err error) {
	statement := "DELETE FROM blog"
	_, err = Db.Exec(statement)
	return
}

// Get all visible posts in the database and returns it
func Blogposts() (posts []Blogpost, err error) {
	rows, err := Db.Query("SELECT id, category, author, message, created_at, updated_at FROM blog WHERE visible = '1' ORDER BY id desc")
	if err != nil {
		return
	}
	for rows.Next() {
		post := Blogpost{}
		if err = rows.Scan(&post.Id, &post.Category, &post.Author, &post.Message, &post.CreatedAt, &post.UpdatedAt); err != nil {
			return
		}
		posts = append(posts, post)
	}
	rows.Close()
	return
}

// Get all visible posts in the database and returns it
func BlogpostsByCategory(category string) (posts []Blogpost, err error) {
	rows, err := Db.Query("SELECT id, category, author, message, created_at, updated_at FROM blog WHERE category = ? AND visible = '1' ORDER BY id desc", category)
	if err != nil {
		return
	}
	for rows.Next() {
		post := Blogpost{}
		if err = rows.Scan(&post.Id, &post.Category, &post.Author, &post.Message, &post.CreatedAt, &post.UpdatedAt); err != nil {
			return
		}
		posts = append(posts, post)
	}
	rows.Close()
	return
}

// Get a single blogpost given the Id
func BlogpostById(id int) (post Blogpost, err error) {
	post = Blogpost{}
	err = Db.QueryRow("SELECT id, category, author, message, visible, created_at, updated_at FROM messages WHERE id = ?", id).
		Scan(&post.Id, &post.Category, &post.Author, &post.Message, &post.Visible, &post.CreatedAt, &post.UpdatedAt)
	return
}
