package data

import (
	"database/sql"
	"testing"
)

func Test_EventByYear(t *testing.T) {
	y := time.Now().Year()
	ev, err := EventByYear(y)
	if err != nil {
		t.Error(err, "Can't get event details.")
	}
}
