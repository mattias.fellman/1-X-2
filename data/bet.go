package data

import (
	"time"
)

type BetObject struct {
	Id          int
	EventId     int
	ExternalId  int
	Name        string
	Type        string
	Value       int
	OccursAt    time.Time
	Winner      string
	WinnerInfo  string
	HomeScore   int
	AwayScore   int
	CorrectBets int
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type Bet struct {
	Id         int64
	SlipId     int64
	ObjectId   int
	Winner     string
	WinnerInfo string
	HomeScore  int
	AwayScore  int
	Correct    bool
	Incorrect  bool
}

// Get all bet objects
func BetObjects(eventId int) (objects []BetObject, err error) {
	rows, err := Db.Query(`
		SELECT id, event_id, external_id, name, type, value, occurs_at, winner, winner_info, home_score, away_score, correct_bets, created_at, updated_at 
		FROM betobjects
		WHERE event_id = ?`, eventId)
	if err != nil {
		return
	}
	for rows.Next() {
		object := BetObject{}
		if err = rows.Scan(&object.Id, &object.EventId, &object.ExternalId, &object.Name, &object.Type, &object.Value, &object.OccursAt, &object.Winner, &object.WinnerInfo, &object.HomeScore, &object.AwayScore, &object.CorrectBets, &object.CreatedAt, &object.UpdatedAt); err != nil {
			return
		}
		objects = append(objects, object)
	}
	rows.Close()
	return
}

// Get next result that can be set
func NextResultToSet(eventId int) (objects []BetObject, err error) {
	rows, err := Db.Query(`SELECT id, event_id, external_id, name, type, value, winner, winner_info, home_score, away_score, correct_bets, created_at, updated_at 
						   FROM betobjects
						   WHERE name = 
						   (
						   		SELECT name FROM betobjects
								WHERE winner = "" AND event_id = ?
								ORDER BY id
								LIMIT 0,1
						   	)
						   	AND event_id = ?
						   	`, eventId, eventId)
	if err != nil {
		return
	}
	for rows.Next() {
		object := BetObject{}
		if err = rows.Scan(&object.Id, &object.EventId, &object.ExternalId, &object.Name, &object.Type, &object.Value, &object.Winner, &object.WinnerInfo, &object.HomeScore, &object.AwayScore, &object.CorrectBets, &object.CreatedAt, &object.UpdatedAt); err != nil {
			return
		}
		objects = append(objects, object)
	}
	rows.Close()
	return
}

// Get betobject by name
func BetObjectIdByName(name string, offset int, eventId int) (betId int, err error) {
	rows, err := Db.Query("SELECT id FROM betobjects WHERE name = ? AND event_id = ? LIMIT ?, 1", name, eventId, offset)
	if err != nil {
		return
	}
	for rows.Next() {
		if err = rows.Scan(&betId); err != nil {
			return
		}
	}
	rows.Close()
	return
}

// Create a new bet
func (bet *Bet) Create() (err error) {
	statement := "INSERT INTO bets (slip_id, object_id, winner, winner_info, home_score, away_score, correct, incorrect) VALUES (?, ?, ?, ?, ?, ?, ?, ?)"
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(bet.SlipId, bet.ObjectId, bet.Winner, bet.WinnerInfo, bet.HomeScore, bet.AwayScore, 0, 0)
	if err != nil {
		return
	}
	return
}

// Set result using object id
func (betObject *BetObject) SetResultUsingId() (err error) {
	statement1 := `UPDATE bets 
		SET 
		correct = IF(winner = ?, 1, 0),
		incorrect = IF(winner = ?, 0, 1)
		WHERE object_id = ?`
	stmt1, err := Db.Prepare(statement1)
	if err != nil {
		return
	}
	defer stmt1.Close()
	_, err = stmt1.Exec(betObject.Winner, betObject.Winner, betObject.Id)
	if err != nil {
		return
	}

	statement2 := `UPDATE betobjects 
		SET 
		winner = ?, 
		winner_info = ?,
		home_score = ?,
		away_score = ?,
		correct_bets = (SELECT count(*) FROM bets WHERE correct = 1 AND object_id = ?),
		updated_at = NOW()
		WHERE id = ?`
	stmt2, err := Db.Prepare(statement2)
	if err != nil {
		return
	}
	defer stmt2.Close()
	_, err = stmt2.Exec(betObject.Winner, betObject.WinnerInfo, betObject.HomeScore, betObject.AwayScore, betObject.Id, betObject.Id)
	if err != nil {
		return
	}
	return
}

// Set result using object id
func (betObject *BetObject) SetResultUsingName() (err error) {
	statement1 := `UPDATE bets b
		SET 
		b.correct = 1
		WHERE b.object_id IN (SELECT o.id FROM betobjects o WHERE o.name = ? AND o.event_id = ?) AND b.winner = ?`
	stmt1, err := Db.Prepare(statement1)
	if err != nil {
		return
	}
	defer stmt1.Close()
	_, err = stmt1.Exec(betObject.Name, betObject.EventId, betObject.Winner)
	if err != nil {
		return
	}

	statement2 := `
	UPDATE betobjects o1
	SET 
	o1.winner = ?, 
	o1.correct_bets = 
		(SELECT count(b.id) FROM bets b WHERE b.correct = 1 AND b.winner = ? AND b.object_id IN 
			(SELECT id FROM (SELECT o2.id FROM betobjects o2 WHERE o2.name = ? AND o2.event_id = ?) tmp)
		),
	o1.updated_at = NOW()
	WHERE 
	o1.name = ? 
    AND o1.id IN (SELECT id FROM (SELECT o3.id FROM betobjects o3 WHERE o3.name = ? AND o3.event_id = ? AND o3.winner = '' LIMIT 0,1) tmp)`
	stmt2, err := Db.Prepare(statement2)
	if err != nil {
		return
	}
	defer stmt2.Close()
	_, err = stmt2.Exec(betObject.Winner, betObject.Winner, betObject.Name, betObject.EventId, betObject.Name, betObject.Name, betObject.EventId)
	if err != nil {
		return
	}
	return
}

func SetIncorrects(event_id int, name string) (err error) {
	statement := `UPDATE bets 
		SET 
		incorrect = 1 
		WHERE correct = 0 
		AND incorrect = 0
		AND object_id IN (SELECT id FROM (SELECT id FROM betobjects WHERE event_id = ? AND name = ?) tmp)`
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(event_id, name)
	if err != nil {
		return
	}
	return
}

// Calculate slip points for every user after new results has been inserted
func CalculatePoints(event_id int) (err error) {
	statement := `UPDATE betslips s
		SET
		s.correct_answers = (SELECT COUNT(*) FROM bets b1 WHERE b1.correct = 1 AND b1.slip_id = s.id),
		s.points = (SELECT SUM(b2.correct * o2.value) FROM bets b2 LEFT JOIN betobjects o2 ON b2.object_id = o2.id WHERE b2.slip_id = s.id)
		WHERE event_id = ?`
	stmt, err := Db.Prepare(statement)
	if err != nil {
		return
	}
	defer stmt.Close()
	_, err = stmt.Exec(event_id)
	if err != nil {
		return
	}
	return
}
