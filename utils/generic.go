package utils

import (
	"regexp"
	"sort"
	"strings"

	"1-X-2/config"
	"1-X-2/data"
	"1-X-2/logger"
)

type Country struct {
	FifaCode    string
	DisplayName string
}

type GroupMatch struct {
	ObjectId int
	HomeVal  string
	AwayVal  string
	Points   int
}

// Sort displayname asc
type ByDisplayName []Country

func (a ByDisplayName) Len() int           { return len(a) }
func (a ByDisplayName) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByDisplayName) Less(i, j int) bool { return a[i].DisplayName < a[j].DisplayName }

// Putting the data for the bet slip -form together
func FormatMatches(bo []data.BetObject) (matches []GroupMatch) {
	// Format max all groupmatches
	i := len(bo)
	if i >= config.Get.GroupMatches {
		i = config.Get.GroupMatches
	}

	for _, v := range bo[:i] {
		tmp := GroupMatch{
			ObjectId: v.Id,
			HomeVal:  v.Name[:3],
			AwayVal:  v.Name[4:],
			Points:   v.Value,
		}
		matches = append(matches, tmp)
	}
	return
}

func SetRoute(url string) (route string) {
	route = config.Get.EntryPage
	if len(url) >= 4 {
		s := strings.Split(url[4:], "\\")
		route = s[0]
	}
	return
}

func GetBetObjectDetails(bo []data.BetObject) (oType string, oCount int) {
	oCount = len(bo)
	if oCount <= 0 {
		return
	}
	if oCount > 1 {
		oType = "checkbox"
		return
	}

	r, _ := regexp.Compile("^([A-Za-z]{3}-[A-Za-z]{3})$")
	if oCount == 1 && r.MatchString(bo[0].Name) {
		oType = "radio"
		return
	}
	selects := []string{"Bronze", "Silver", "Gold"}
	if StringInSlice(bo[0].Name, selects) {
		oType = "select"
		return
	}
	oType = "text"
	return
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// Get list of countries from Group Stage matches
func GetCountries(eventId int, translation map[string]string) (countries []Country) {
	bo, err := data.BetObjects(eventId)
	if err != nil {
		logger.Danger(err, "Cannot get bet objects to build list of countries")
	}
	for _, v := range bo[:config.Get.GroupMatches] {
		code := v.Name[:3]
		if !codeInCountry(code, countries) {
			tmp := Country{
				FifaCode:    code,
				DisplayName: translation[v.Name[:3]],
			}
			countries = append(countries, tmp)
		}
	}
	sort.Sort(ByDisplayName(countries))
	return
}

func codeInCountry(a string, c []Country) bool {
	for _, v := range c {
		if v.FifaCode == a {
			return true
		}
	}
	return false
}
