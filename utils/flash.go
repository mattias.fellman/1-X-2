package utils

import (
	"encoding/base64"
	"encoding/json"
	"net/http"
	"time"
)

type Alert struct {
	Type    string
	Heading string
	Message string
}

const CookieName = "_flashcookie"

func SetFlash(writer http.ResponseWriter, alert Alert) {
	cookie := &http.Cookie{Name: CookieName, Value: encode(alert)}
	http.SetCookie(writer, cookie)
}

func GetFlash(writer http.ResponseWriter, request *http.Request) (*Alert, error) {
	cookie, err := request.Cookie(CookieName)
	if err != nil {
		switch err {
		case http.ErrNoCookie:
			return nil, nil
		default:
			return nil, err
		}
	}
	value, err := decode(cookie.Value)
	if err != nil {
		return nil, err
	}
	delCookie := &http.Cookie{Name: CookieName, MaxAge: -1, Expires: time.Unix(1, 0)}
	http.SetCookie(writer, delCookie)
	return value, nil
}

func encode(alert Alert) string {
	src, err := json.Marshal(alert)
	if err != nil {
		return ""
	}
	return base64.StdEncoding.EncodeToString(src)
}

func decode(src string) (*Alert, error) {
	data, err := base64.StdEncoding.DecodeString(src)
	if err != nil {
		return nil, err
	}
	var msg *Alert
	if err := json.Unmarshal(data, &msg); err != nil {
		return nil, err
	}
	return msg, err
}
