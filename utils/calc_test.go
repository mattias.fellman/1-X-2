package utils

import (
	"testing"
)

func Test_CalcRatio(t *testing.T) {
	num1, num2 := 0, 1
	ratio := CalcRatio(num1, num2)
	if ratio != 0 {
		t.Error("Ratio calculation fails on zero division")
	}

	num1, num2 = 50, 100
	ratio = CalcRatio(num1, num2)
	if ratio != 0.5 {
		t.Error("Ratio calculation does not return correct result")
	}

	num1, num2 = 150, 75
	ratio = CalcRatio(num1, num2)
	if ratio != 2 {
		t.Error("Ratio calculation does not return correct result")
	}
}

func Test_CalcPrizeMoney(t *testing.T) {
	bets, cost, free := 100, 10, 2
	prize := CalcPrizeMoney(bets, cost, free)
	if prize != 980 {
		t.Error("Prize money calculation returned wrong result, should be 980")
	}

	bets, cost, free = 264, 5, 3
	prize = CalcPrizeMoney(bets, cost, free)
	if prize != 1305 {
		t.Error("Prize money calculation returned wrong result, should be 1305")
	}
}
