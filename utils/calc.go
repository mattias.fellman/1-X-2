package utils

import (
	"math"
)

func CalcRatio(num1 int, num2 int) (ratio float64) {
	if num1 == 0 || num2 == 0 {
		ratio = 0
		return
	}

	r := float64(num1) / float64(num2)
	ratio = math.Round(r*100) / 100
	return
}

func CalcPrizeMoney(bets int, cost int, free int) (prize int) {
	// Two free bets for the arrangers
	prize = bets*cost - (cost * free)
	return
}
