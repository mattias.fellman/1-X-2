package main

import (
	"1-X-2/auth"
	"1-X-2/config"
	"1-X-2/logger"
	"1-X-2/utils"
	"fmt"
	"net/http"
	"path/filepath"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

// Log each request
func logWrapper(handler http.HandlerFunc) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		t1 := time.Now()
		handler(w, r)
		t2 := time.Now()
		str := fmt.Sprintf("%s %q %v", r.Method, r.URL.String(), t2.Sub(t1))
		logger.P(str)
	}
	return http.HandlerFunc(fn)
}

// Initialise base information
func setContent(handler http.HandlerFunc, accessLevel int) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		args := mux.Vars(r)
		url := filepath.Clean(r.URL.String())

		content.Lang = args["lang"]
		content.Route = utils.SetRoute(url)
		content.Translation = config.Translations(content.Lang)

		params := map[string]string{
			"Lang":    content.Lang,
			"Heading": content.Translation["Oops"],
			"Message": content.Translation["SessionExpired"],
		}
		content.UserId, content.AccessLevel, _ = auth.Authorize(w, r, accessLevel, params)
		content.Alert, _ = utils.GetFlash(w, r)
		content.EndIsNear = false
		content.NotFound = false
		content.Navbar = "navbar." + strconv.Itoa(content.AccessLevel)
		if content.AccessLevel > 0 {
			config.EventDetails()
		}
		handler(w, r)
	}
	return logWrapper(fn)
}

func main() {
	logger.P("1-X-2", config.Version(), "started at", config.Get.Address)

	// routing setup and handle static resources
	rtr := mux.NewRouter()
	s := http.StripPrefix("/static/", http.FileServer(http.Dir("static")))
	rtr.PathPrefix("/static/").Handler(s)

	// Route patterns matched here while handler functions defined in other files
	langParam := "{lang:[a-z]{2}}"

	// Access level should be set for every page
	// 0 = none (not authenticated)
	// 1 = medium (registered user)
	// 2 = high (admin)

	// Defined in route_main.go
	rtr.HandleFunc("/", index)
	rtr.HandleFunc("/"+langParam+"/err", setContent(err, 0))
	rtr.HandleFunc("/"+langParam+"/about", setContent(about, 0))
	rtr.HandleFunc("/"+langParam+"/info", setContent(infoPage, 0))
	rtr.HandleFunc("/"+langParam+"/password-forgotten", setContent(passwordForgotten, 0))
	rtr.HandleFunc("/"+langParam+"/rules", setContent(rules, 0))

	// Defined in route_auth.go
	rtr.HandleFunc("/"+langParam+"/login", setContent(login, 0))
	rtr.HandleFunc("/"+langParam+"/logout", setContent(logout, 0))
	rtr.HandleFunc("/"+langParam+"/signup", setContent(signup, 0))
	rtr.HandleFunc("/"+langParam+"/signup-account", setContent(signupAccount, 0))
	rtr.HandleFunc("/"+langParam+"/authenticate", setContent(authenticate, 0))

	// Defined in route_private.go
	rtr.HandleFunc("/"+langParam+"/bets", setContent(bets, 1))
	rtr.HandleFunc("/"+langParam+"/bets/{slip}", setContent(getBetSlip, 1))
	rtr.HandleFunc("/"+langParam+"/betslip", setContent(betSlipForm, 1))
	rtr.HandleFunc("/"+langParam+"/standing", setContent(standing, 1))
	rtr.HandleFunc("/"+langParam+"/stats", setContent(stats, 1))
	rtr.HandleFunc("/"+langParam+"/news", setContent(news, 1))

	// Defined in route_admin.go
	rtr.HandleFunc("/"+langParam+"/admin/reset", setContent(reset, 2))
	rtr.HandleFunc("/"+langParam+"/admin/result", setContent(result, 2))
	rtr.HandleFunc("/"+langParam+"/admin/approve", setContent(approve, 2))
	rtr.HandleFunc("/"+langParam+"/admin/set-result", setContent(setResult, 2))
	rtr.HandleFunc("/"+langParam+"/admin/reset-event", setContent(resetEvent, 2))
	rtr.HandleFunc("/"+langParam+"/admin/blog", setContent(adminBlog, 2))
	rtr.HandleFunc("/"+langParam+"/admin/blogpost-create", setContent(createBlogpost, 2))

	// Defined in route_ajax.go
	rtr.HandleFunc("/"+langParam+"/pseudonyms", setContent(checkPseud, 1))
	rtr.HandleFunc("/"+langParam+"/betslip-create", setContent(createBetSlip, 1))
	rtr.HandleFunc("/"+langParam+"/admin/set-betslip-status", setContent(setBetSlipStatus, 2))
	rtr.HandleFunc("/"+langParam+"/admin/blogpost-hide", setContent(hideBlogpost, 2))

	// Starting up the server
	server := &http.Server{
		Addr:           config.Get.Address,
		Handler:        rtr,
		ReadTimeout:    time.Duration(config.Get.ReadTimeout * int64(time.Second)),
		WriteTimeout:   time.Duration(config.Get.WriteTimeout * int64(time.Second)),
		MaxHeaderBytes: 1 << 20,
	}
	server.ListenAndServe()
}
