package main

import (
	"1-X-2/config"
	"1-X-2/utils"
	"fmt"
	"html/template"
	"net/http"
	"strings"
	"time"
)

type WebContent struct {
	Lang        string
	Route       string
	AccessLevel int
	Navbar      string
	UserId      int
	EndIsNear   bool
	NotFound    bool
	Translation map[string]string
	Data        interface{}
	Alert       *utils.Alert
}

var content WebContent

func generateHTML(writer http.ResponseWriter, data interface{}, filenames ...string) {
	fmap := template.FuncMap{
		"mod":            mod,
		"add":            add,
		"html":           html,
		"translate":      translate,
		"betStatus":      betStatus,
		"formatDateTime": formatDateTime,
	}

	var files = []string{"templates/layout.html"}
	for _, file := range filenames {
		files = append(files, fmt.Sprintf("templates/%s.html", file))
	}
	templates := template.Must(template.New("").Funcs(fmap).ParseFiles(files...))
	templates.ExecuteTemplate(writer, "layout", data)
}

func mod(i, j int) bool {
	return i%j == 0
}

func add(i, j int) int {
	return i + j
}
func formatDateTime(t time.Time) string {
	loc, _ := time.LoadLocation(config.Get.TimeZone)
	return t.In(loc).Format("02.01.2006 15:04")
}

func betStatus(a bool, d bool) string {
	if d {
		return content.Translation["StatusDiscarded"]
	} else if a {
		return content.Translation["StatusApproved"]
	} else {
		return content.Translation["StatusPending"]
	}
}

func translate(s string) string {
	if _, ok := content.Translation[s]; ok {
		return content.Translation[s]
	} else {
		// Country codes splitting
		if strings.Contains(s, "-") && len(s) == 7 {
			s = content.Translation[s[:3]] + " - " + content.Translation[s[4:]]
		}
		return s
	}
}

func html(value interface{}) template.HTML {
	return template.HTML(fmt.Sprint(value))
}
