# 1-X-2 betting system

This is the code repository for a betting system for an invited group of people.

Built this far using.
* Go 1.10 - https://golang.org/ 
* MySQL 5.7 - https://www.mysql.com/
* JQuery - https://jquery.com/
* Bootstrap - http://getbootstrap.com/
* Bootswatch - https://bootswatch.com/

# Free to use, change and abuse
But no guarantees given, use it at your own peril or benefit.

## Get a local copy running
* Prerequisites: Go and MySQL locally installed (can be ported to another db)
* git clone https://gitlab.com/mattias.fellman/1-X-2.git
* Modify the config.json -file
* Create database schema
* Run setup_mysql.sql

#### Test setup
* go build "list all go files in rooth directory starting with main.go"
* As of 2018-06-26: main.go content.go route_main.go route_auth.go route_admin.go route_private.go route_ajax.go
* Execute main -executable (main.exe on Windows)
* Open browser and surf to http://localhost:8080/en/about


## Deploying to Google Cloud Platform
* Sign up to Google Cloud Platform
* Create a project
* Create a Google App Engine
* Create cloud SQL database (micro flavor to save costs...) 
* Download and install Google Cloud SDK Shell   
* Create an app.yaml -file (see below)

###### app.yaml should be in the root folder of the application.
```yaml
runtime: go
env: flex

skip_files:
- app.log
- README.md
- main.exe

automatic_scaling:
  min_num_instances: 2
  max_num_instances: 6
  cool_down_period_sec: 120
  cpu_utilization:
    target_utilization: 0.6

resources:
  cpu: 1
  memory_gb: 1

#[START env]
env_variables:
  # See https://github.com/go-sql-driver/mysql
  #
  MYSQL_CONNECTION: user:password@unix(/cloudsql/project:region:instance)/speruna?parseTime=true
  #
  # If you're testing locally using the Cloud SQL proxy with TCP,
  # instead use the "tcp" dialer by setting the environment variable:
  # MYSQL_CONNECTION=user:password@tcp(104.xxx.xxx.xxx:3306)/dbname?parseTime=true
#[END env]

#[START cloudsql_settings]
# Replace INSTANCE_CONNECTION_NAME with the value obtained when configuring your
# Cloud SQL instance, available from the Google Cloud Console or from the Cloud SDK.
# For SQL v2 instances, this should be in the form of "project:region:instance".
# Cloud SQL v1 instances are not supported.
beta_settings:
  cloud_sql_instances: project:region:instance
#[END cloudsql_settings]
```

#### Deploy with Google Cloud SDK Shell
* Start shell
* move to root folder of the application
* gcloud config list (make sure settings are correct)
* gcloud app deploy (wait)
* gcloud app browse (test)
