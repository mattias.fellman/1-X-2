package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"time"

	"1-X-2/data"
	"1-X-2/logger"
)

type Configuration struct {
	Address       string
	ReadTimeout   int64
	WriteTimeout  int64
	CookieName    string
	EntryPage     string
	DefaultLang   string
	Invitation    string
	TimeZone      string
	BetSlipCost   int
	FreeBetSlips  int
	EventId       int
	GroupMatches  int
	BettingStarts time.Time
	BettingEnds   time.Time
}

var Get Configuration

func init() {
	loadConfig()
}

func loadConfig() {
	path := "./config/config.json"
	file, err := ioutil.ReadFile(path)
	if err != nil {
		logger.P("Config File Missing: ", err)
	}

	err = json.Unmarshal(file, &Get)
	if err != nil {
		logger.P("Config Parse Error: ", err)
	}
}

func EventDetails() {
	y := time.Now().Year()
	ev, err := data.EventByYear(y)
	if err != nil {
		logger.Danger(err, "Not getting event configs")
	}
	Get.EventId = ev.Id
	Get.GroupMatches = ev.GroupMatches
	Get.BettingStarts = ev.BettingStarts
	Get.BettingEnds = ev.BettingEnds
}

func Translations(lang string) map[string]string {
	translation := make(map[string]string)
	files := []string{"texts", "geo"}
	for _, file := range files {
		f := loadTranslationFile(file + "." + lang + ".json")
		for k, v := range f {
			translation[k] = v
		}
	}
	return translation
}

func loadTranslationFile(fName string) map[string]string {
	file, err := os.Open("./translations/" + fName)
	defer file.Close()

	byteValue, _ := ioutil.ReadAll(file)
	translation := map[string]string{}

	err = json.Unmarshal([]byte(byteValue), &translation)
	if err != nil {
		panic(err)
	}
	return translation
}

func Version() string {
	return "0.2"
}
