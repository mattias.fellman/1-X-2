package auth

import (
	"1-X-2/data"
	"1-X-2/utils"
	"errors"
	"net/http"
)

type UserSess struct {
	UserId int
}

type Notice struct {
	Type    string
	Heading string
	Message string
}

func Authorize(w http.ResponseWriter, r *http.Request, accessLevel int, args map[string]string) (userId int, role int, err error) {
	sess, err := Session(w, r)
	role = 0

	if sess.Role > 0 {
		role = sess.Role
		userId = sess.UserId
	}

	if role >= accessLevel {
		return
	}

	// Fall through, deny if not specifically granted
	if len(sess.Uuid) > 0 {
		sess.DeleteByUUID()
	}

	alert := utils.Alert{
		Type:    "warning",
		Heading: args["Heading"],
		Message: args["Message"],
	}
	utils.SetFlash(w, alert)
	http.Redirect(w, r, "/"+args["Lang"]+"/login", 302)
	return
}

// Checks if the user is logged in and has a session, if not err is not nil
func Session(w http.ResponseWriter, r *http.Request) (sess data.Session, err error) {
	cookie, err := r.Cookie("_cookie")
	if err == nil {
		sess = data.Session{Uuid: cookie.Value}
		if ok, _ := sess.Check(); !ok {
			err = errors.New("Invalid session")
		}
	}
	return
}
