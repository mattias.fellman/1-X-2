package main

import (
	"1-X-2/auth"
	"1-X-2/config"
	"1-X-2/data"
	"1-X-2/logger"
	"1-X-2/utils"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type FormData struct {
	EventId   int
	Matches   []utils.GroupMatch
	Countries []utils.Country
}

type BetData struct {
	Slip data.BetSlip
	Bets []data.BetJoined
}

type Standing struct {
	Id             int64
	Rank           int
	Pseudonym      string
	CorrectAnswers int
	Points         int
}

type Stats struct {
	BetStats            []data.Stat
	NumUsers            int
	BetSlipUserRatio    float64
	NumApprovedBetSlips int
	NumPendingBetSlips  int
	PrizeMoney          int
}

// GET /bets
// Shows users bet slips
func bets(w http.ResponseWriter, r *http.Request) {
	sess, err := auth.Session(w, r)
	user, err := sess.User()
	if err != nil {
		logger.Danger(err, "Cannot get user from session")
	}
	slips, err := user.BetSlipsByUser()
	if err != nil {
		logger.Danger(err, "Cannot get bet slips for user")
	}
	content.Data = slips
	generateHTML(w, content, content.Navbar, content.Route)
}

// GET /bets/{id}
// Shows bet slip
func getBetSlip(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	slipId, err := strconv.Atoi(params["slip"])
	if err != nil {
		content.NotFound = true
		content.Data = nil
		generateHTML(w, content, content.Navbar, "betslip")
		return
	}

	slip, err := data.BetSlipById(slipId)
	if err != nil {
		content.NotFound = true
		content.Data = nil
		generateHTML(w, content, content.Navbar, "betslip")
		return
	}
	bets, err := data.BetsBySlipId(slip.Id)
	if err != nil {
		logger.Danger(err, "Cannot get bets for slip")
	}

	betData := BetData{}
	betData.Slip = slip
	betData.Bets = bets
	content.Data = betData
	generateHTML(w, content, content.Navbar, "betslip")
}

// GET /betslip
// Shows bet slip input form
func betSlipForm(w http.ResponseWriter, r *http.Request) {
	// Prevent betting after tournament start
	now := time.Now().UTC()
	if now.Before(config.Get.BettingStarts) || now.After(config.Get.BettingEnds) {
		generateHTML(w, content, content.Navbar, "betslip.closed")
	} else {
		bo, err := data.BetObjects(config.Get.EventId)
		if err != nil {
			logger.Danger(err, "Cannot get bet objects for slip")
		}

		formData := FormData{}
		formData.EventId = bo[0].EventId
		formData.Matches = utils.FormatMatches(bo)
		formData.Countries = utils.GetCountries(config.Get.EventId, content.Translation)
		content.Data = formData
		// Warning that bet slip is closing
		if now.After(config.Get.BettingEnds.Add(time.Hour * -2)) {
			content.EndIsNear = true
		}

		generateHTML(w, content, content.Navbar, content.Route+".form")
	}
}

// GET /standings
// Shows the current standing
func standing(w http.ResponseWriter, r *http.Request) {
	slips, err := data.Standings(config.Get.EventId)
	if err != nil {
		logger.Danger(err, "Cannot get slip standing")
	}
	standings := []Standing{}
	points, rank, slipsPerRank := 0, 1, 0
	for _, v := range slips {
		s := Standing{}
		s.Id = v.Id
		s.Pseudonym = v.Pseudonym
		s.Rank = rank
		if v.Points < points {
			rank += slipsPerRank
			s.Rank = rank
			slipsPerRank = 1
		} else {
			slipsPerRank += 1
		}

		s.Points, points = v.Points, v.Points
		standings = append(standings, s)
	}

	content.Data = standings
	generateHTML(w, content, content.Navbar, content.Route)
}

// GET /stats
// Shows the knockout statistics
func stats(w http.ResponseWriter, r *http.Request) {
	resultStats, err := data.KnockoutStats(config.Get.EventId)
	if err != nil {
		logger.Danger(err, "Cannot get knockout statistics")
	}

	activeUsers, err := data.NumUsers()
	if err != nil {
		logger.Danger(err, "Cannot get user count")
	}

	approvedBetSlips, err := data.NumBetSlips(1, 0, config.Get.EventId)
	if err != nil {
		logger.Danger(err, "Cannot get approved bet slip count")
	}

	pendingBetSlips, err := data.NumBetSlips(0, 0, config.Get.EventId)
	if err != nil {
		logger.Danger(err, "Cannot get pending bet slip count")
	}

	stats := Stats{}
	stats.BetStats = resultStats
	stats.NumUsers = activeUsers
	stats.BetSlipUserRatio = utils.CalcRatio(approvedBetSlips+pendingBetSlips, activeUsers)
	stats.NumApprovedBetSlips = approvedBetSlips
	stats.NumPendingBetSlips = pendingBetSlips
	stats.PrizeMoney = utils.CalcPrizeMoney(approvedBetSlips, config.Get.BetSlipCost, config.Get.FreeBetSlips)

	content.Data = stats
	generateHTML(w, content, content.Navbar, content.Route)
}

// GET /news
// Shows the fact blog messages
func news(w http.ResponseWriter, r *http.Request) {
	posts, err := data.BlogpostsByCategory("BlogFact")
	if err != nil {
		logger.Danger(err, "Cannot get blog posts in the BlogFact category")
	}
	content.Data = posts
	generateHTML(w, content, content.Navbar, content.Route)
}
