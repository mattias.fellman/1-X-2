package main

import (
	"1-X-2/config"
	"1-X-2/utils"
	"net/http"
)

// GET /
// Redirect to entry page
func index(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/"+config.Get.DefaultLang+"/"+config.Get.EntryPage, 302)
}

// GET /err
// shows the error message page
func err(w http.ResponseWriter, r *http.Request) {
	generateHTML(w, content, content.Navbar, "error")
}

// Convenience function to redirect to the error message page
func errorMessage(w http.ResponseWriter, r *http.Request, msg string) {
	alert := utils.Alert{
		Type:    "warning",
		Heading: content.Translation["Oops"],
		Message: msg,
	}
	utils.SetFlash(w, alert)
	http.Redirect(w, r, "/"+content.Lang+"/err", 302)
}

// GET /about
// Shows the brief info about this site
func about(w http.ResponseWriter, r *http.Request) {
	template := "about." + content.Lang
	generateHTML(w, content, content.Navbar, template)
}

// GET /info
// Shows tournament info
func infoPage(w http.ResponseWriter, r *http.Request) {
	generateHTML(w, content, content.Navbar, "info")
}

// GET /password-forgotten
// Shows what to do when the password has been forgotten
func passwordForgotten(w http.ResponseWriter, r *http.Request) {
	template := "password-forgotten"
	generateHTML(w, content, content.Navbar, template)
}

// GET /rules
// Shows the betting rules
func rules(w http.ResponseWriter, r *http.Request) {
	generateHTML(w, content, content.Navbar, "rules")
}
