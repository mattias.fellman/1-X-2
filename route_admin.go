package main

import (
	"1-X-2/config"
	"1-X-2/data"
	"1-X-2/logger"
	"1-X-2/utils"
	"net/http"
	"strconv"
	"strings"
)

type ResultData struct {
	EventId   int
	ObjectId  int
	Name      string
	Title     string
	Label     string
	Count     int
	Matches   []utils.GroupMatch
	Countries []utils.Country
}

// GET /approvals
// Shows list of bet slips that can be approved
func approve(w http.ResponseWriter, r *http.Request) {
	var err error
	content.Data, err = data.BetSlipsToApprove(config.Get.EventId)
	if err != nil {
		logger.Danger(err, "Cannot fetch bet slips")
		errorMessage(w, r, "Cannot fetch slips for approval")
	} else {
		generateHTML(w, content, content.Navbar, "admin.approve")
	}
}

// GET /admin/reset
// Shows reset page
func reset(w http.ResponseWriter, r *http.Request) {
	generateHTML(w, content, content.Navbar, "admin.reset")
}

// POST /reset-results
// Resets the event
func resetEvent(w http.ResponseWriter, r *http.Request) {
	err := data.ResetEvent(config.Get.EventId)
	alert := utils.Alert{
		Type:    "success",
		Heading: content.Translation["Success"],
		Message: content.Translation["EventReset"],
	}
	if err != nil {
		logger.Danger(err, "Could not reset event")
		http.Error(w, err.Error(), http.StatusInternalServerError)

		alert.Type = "warning"
		alert.Heading = content.Translation["Failed"]
		alert.Message = content.Translation["EventNotReset"]
	}

	utils.SetFlash(w, alert)
	http.Redirect(w, r, "/"+content.Lang+"/admin/reset", 302)
	return
}

// GET /admin/result
// Shows the results that can be set
func result(w http.ResponseWriter, r *http.Request) {
	bo, err := data.NextResultToSet(config.Get.EventId)
	if err != nil {
		logger.Danger(err, "Cannot get result that can be set")
	}

	oType, oCount := utils.GetBetObjectDetails(bo)
	if oCount == 0 {
		generateHTML(w, content, content.Navbar, "result.none")
		return
	}

	rd := ResultData{}
	rd.Count = oCount
	rd.EventId = bo[0].EventId
	rd.Name = bo[0].Name
	rd.ObjectId = bo[0].Id

	switch oType {
	case "checkbox":
		rd.Title = content.Translation["Knockout"+strconv.Itoa(oCount)]
		rd.Label = content.Translation["Select"+strconv.Itoa(oCount)]
		rd.Countries = utils.GetCountries(config.Get.EventId, content.Translation)
	case "radio":
		rd.Matches = utils.FormatMatches(bo)
	case "select":
		rd.Label = content.Translation[bo[0].Name]
		rd.Countries = utils.GetCountries(config.Get.EventId, content.Translation)
	case "text":
		rd.Label = content.Translation[bo[0].Name]
	}

	content.Data = rd
	generateHTML(w, content, content.Navbar, "result."+oType)
}

// POST /admin/set-result
func setResult(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		logger.Danger(err, "Cannot parse form")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	eventId, err := strconv.Atoi(r.PostFormValue("EventId"))
	if err != nil {
		logger.Danger(err, "Don't know which event")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	for key, val := range r.PostForm {
		if strings.HasPrefix(key, "winner.") {
			objectId, _ := strconv.Atoi(key[7:])
			bo := data.BetObject{
				Id:     objectId,
				Winner: r.Form.Get(key),
			}
			if err := bo.SetResultUsingId(); err != nil {
				logger.Danger(err, "Cannot insert match winner using object id")
			}
			continue
		}

		if key == "Knockout" {
			objectName := r.PostFormValue("ObjectName")
			for _, v := range val {
				bo := data.BetObject{
					Name:    objectName,
					EventId: eventId,
					Winner:  v,
				}
				if err := bo.SetResultUsingName(); err != nil {
					logger.Danger(err, "Cannot insert match winner using object name")
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
			}
			if err := data.SetIncorrects(eventId, objectName); err != nil {
				logger.Danger(err, "Could not set incorrects for knockout phase")
			}
		}
	}

	if err := data.CalculatePoints(eventId); err != nil {
		logger.Danger(err, "Could not calculate points or correct answers")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	alert := utils.Alert{
		Type:    "success",
		Heading: content.Translation["Success"],
		Message: content.Translation["ResultSaved"],
	}
	utils.SetFlash(w, alert)
	http.Redirect(w, r, "/"+content.Lang+"/admin/result", 302)
	return
}

// GET /admin/blog
func adminBlog(w http.ResponseWriter, r *http.Request) {
	posts, err := data.Blogposts()
	if err != nil {
		logger.Danger(err, "Cannot get blog posts that can be set")
	}
	content.Data = posts
	generateHTML(w, content, content.Navbar, "admin.blog")
}

// Post /admin/blogpost-create
func createBlogpost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		logger.Danger(err, "Cannot parse form")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	user, err := data.UserById(content.UserId)
	if err != nil {
		logger.Danger(err, "Cannot get user by id")
	}

	post := data.Blogpost{
		Category: r.PostFormValue("category"),
		Author:   user.Name,
		Message:  r.PostFormValue("message"),
		Visible:  true,
	}

	if err := post.Create(); err != nil {
		logger.Danger(err, "Cannot create blogpost")
	}

	alert := utils.Alert{
		Type:    "success",
		Heading: content.Translation["Success"],
		Message: content.Translation["BlogpostCreated"],
	}
	utils.SetFlash(w, alert)
	http.Redirect(w, r, "/"+content.Lang+"/admin/blog", 302)
	return
}
