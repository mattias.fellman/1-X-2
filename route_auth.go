package main

import (
	"1-X-2/config"
	"1-X-2/data"
	"1-X-2/logger"
	"1-X-2/utils"
	"net/http"
	"strings"
)

// GET /login
// Show the login page
func login(w http.ResponseWriter, r *http.Request) {
	generateHTML(w, content, content.Navbar, "login")
}

// GET /signup
// Show the signup page
func signup(w http.ResponseWriter, r *http.Request) {
	generateHTML(w, content, content.Navbar, "signup")
}

// POST /signup
// Create the user account
func signupAccount(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		logger.Danger(err, "Cannot parse form")
	}

	invitation := strings.ToLower(r.PostFormValue("invitation"))
	if invitation != config.Get.Invitation {
		alert := utils.Alert{
			Type:    "warning",
			Heading: content.Translation["Failed"],
			Message: content.Translation["SignUpInvalidInvitation"],
		}
		utils.SetFlash(w, alert)
		http.Redirect(w, r, "/"+content.Lang+"/signup", 302)
		return
	}

	user := data.User{
		Name:     r.PostFormValue("name"),
		Email:    r.PostFormValue("email"),
		Password: r.PostFormValue("password"),
		Role:     1,
		Active:   true,
	}

	if err := user.Create(); err != nil {
		logger.Danger(err, "Cannot create user")
		alert := utils.Alert{
			Type:    "warning",
			Heading: content.Translation["Failed"],
			Message: content.Translation["SignUpInvalidInput"],
		}
		utils.SetFlash(w, alert)
		http.Redirect(w, r, "/"+content.Lang+"/signup", 302)
		return
	}

	alert := utils.Alert{
		Type:    "success",
		Heading: content.Translation["Success"],
		Message: content.Translation["SignUpSuccess"],
	}
	utils.SetFlash(w, alert)
	http.Redirect(w, r, "/"+content.Lang+"/login", 302)
}

// POST /authenticate
// Authenticate the user given the email and password
func authenticate(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	user, err := data.UserByEmail(r.PostFormValue("email"))
	if err != nil {
		logger.Danger(err, "Cannot find user")
	}
	if user.Password == data.Encrypt(r.PostFormValue("password")) {
		session, err := user.CreateSession()
		if err != nil {
			logger.Danger(err, "Cannot create session")
		}
		cookie := http.Cookie{
			Name:     "_cookie",
			Value:    session.Uuid,
			Path:     "/",
			HttpOnly: true,
		}
		http.SetCookie(w, &cookie)
		http.Redirect(w, r, "/"+content.Lang+"/news", 302)
	} else {

		alert := utils.Alert{
			Type:    "warning",
			Heading: content.Translation["Failed"],
			Message: content.Translation["LoginFailed"],
		}
		utils.SetFlash(w, alert)
		http.Redirect(w, r, "/"+content.Lang+"/login", 302)
	}

}

// GET /logout
// Logs the user out
func logout(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie("_cookie")
	if err != http.ErrNoCookie {
		sess := data.Session{Uuid: cookie.Value}
		sess.DeleteByUUID()
		data.SessionDeleteOld()
	} else {
		logger.Warning(err, "Failed to get cookie")
	}

	alert := utils.Alert{
		Type:    "success",
		Heading: content.Translation["LoggedOut"],
		Message: content.Translation["WelcomeBackMsg"],
	}
	utils.SetFlash(w, alert)
	http.Redirect(w, r, "/"+content.Lang+"/login", 302)
}
