package main

import (
	"1-X-2/config"
	"1-X-2/data"
	"1-X-2/logger"
	"1-X-2/utils"
	"encoding/json"
	"net/http"
	"reflect"
	"strconv"
	"strings"
)

type AjaxResp struct {
	Result  string
	Message string
}

// POST /pseudonyms
func checkPseud(w http.ResponseWriter, r *http.Request) {
	resp := "true"
	msg := content.Translation["PseudFree"]

	pseud := r.PostFormValue("Pseud")
	reserved, err := data.ReservedPseudonym(pseud, config.Get.EventId)
	if err != nil {
		logger.Danger(err, "Cannot check if pseudonym is reserved")
	}
	if reserved == true {
		resp = "false"
		msg = content.Translation["PseudTaken"]
	}

	aj := AjaxResp{
		Result:  resp,
		Message: msg,
	}
	output, err := json.Marshal(aj)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	w.Write(output)
}

// POST /bets
// Shows bet slip
func createBetSlip(w http.ResponseWriter, r *http.Request) {
	eventId, err := strconv.Atoi(r.PostFormValue("EventId"))
	pseud := r.PostFormValue("Pseud")
	reserved, err := data.ReservedPseudonym(pseud, eventId)
	if err != nil {
		logger.Danger(err, "Cannot check if pseudonym is reserved")
	}
	if reserved == true {
		http.Error(w, err.Error(), http.StatusPreconditionFailed)
	}

	slip := data.BetSlip{
		UserId:    content.UserId,
		EventId:   eventId,
		Pseudonym: pseud,
	}
	slipId, err := slip.Create()
	if err != nil {
		logger.Danger(err, "Cannot create bet slip")
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	knockouts := []string{"RoundOf16", "Quarterfinal", "Semifinal", "Bronze", "Silver", "Gold", "TopScorer", "RefereeInFinal"}
	for key, val := range r.Form {
		if strings.HasPrefix(key, "match.") {
			objectId, _ := strconv.Atoi(key[6:])
			bet := data.Bet{
				SlipId:   slipId,
				ObjectId: objectId,
				Winner:   r.Form.Get(key),
			}
			if err := bet.Create(); err != nil {
				logger.Danger(err, "Cannot insert match bet")
			}
		}

		// Checking knockout stage
		// Strings are the one value fields, gold, silver etc
		// Slices comes from the checkboxes
		if utils.StringInSlice(key, knockouts) {
			rt := reflect.TypeOf(val)
			switch rt.Kind() {
			case reflect.String:
				objectId, err := data.BetObjectIdByName(key, 0, config.Get.EventId)
				if err != nil {
					logger.Danger(err, "Cannot get object id")
					continue
				}
				bet := data.Bet{
					SlipId:   slipId,
					ObjectId: objectId,
					Winner:   r.PostFormValue(key),
				}
				if err := bet.Create(); err != nil {
					logger.Danger(err, "Cannot insert final 5 bets")
				}
			case reflect.Slice:
				for k, v := range val {
					objectId, err := data.BetObjectIdByName(key, k, config.Get.EventId)
					if err != nil {
						logger.Danger(err, "Cannot get object id for "+key)
						continue
					}
					bet := data.Bet{
						SlipId:   slipId,
						ObjectId: objectId,
						Winner:   v,
					}
					if err := bet.Create(); err != nil {
						logger.Danger(err, "Cannot insert knockout bet "+key)
					}
				}
			default:
				continue
			}
		}
	}

	aj := AjaxResp{
		Result:  "true",
		Message: "Created slip id: " + strconv.FormatInt(slipId, 10),
	}
	output, err := json.Marshal(aj)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	w.Write(output)
}

// POST /betslip-status
// Approves or discards one bet slip
func setBetSlipStatus(w http.ResponseWriter, r *http.Request) {
	validStatuses := []string{"Approve", "Discard"}
	newStatus := r.PostFormValue("NewStatus")
	if !utils.StringInSlice(newStatus, validStatuses) {
		http.Error(w, "Invalid Status", http.StatusBadRequest)
		return
	}
	slipId, err := strconv.Atoi(r.PostFormValue("SlipId"))
	if err != nil {
		http.Error(w, "Invalid Status", http.StatusBadRequest)
		return
	}

	slip := data.BetSlip{Id: int64(slipId)}
	if newStatus == validStatuses[0] {
		err = slip.Approve()
	}
	if newStatus == validStatuses[1] {
		err = slip.Discard()
	}
	if err != nil {
		logger.Danger(err, "Could not approve slip")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	aj := AjaxResp{
		Result:  "true",
		Message: content.Translation["Status"+strings.Title(newStatus)] + ": " + strconv.FormatInt(slip.Id, 10),
	}
	output, err := json.Marshal(aj)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(output)
}

// Post /admin/blogpost-hide
func hideBlogpost(w http.ResponseWriter, r *http.Request) {
	err := r.ParseForm()
	if err != nil {
		logger.Danger(err, "Cannot parse form")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	betslipId, err := strconv.Atoi(r.PostFormValue("BlogpostId"))
	if err != nil {
		http.Error(w, "Invalid Status", http.StatusBadRequest)
		return
	}

	post := data.Blogpost{
		Id: betslipId,
	}
	if err := post.Hide(); err != nil {
		logger.Danger(err, "Cannot hide blogpost")
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte("OK"))
	return
}
