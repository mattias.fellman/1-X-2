ASIEN

Fahad Al-Mirdasi (KSA)
Alireza Faghani (IRN)
Ravshan Irmatov (UZB)
Mohammed Abdulla Hassan Mohamed (UAE)
Ruyji Sato (JPN)
Nawaf Shukralla (BHR)


AFRIKA

Mehdi Abid Charef (ALG)
Maland Diedhiou (SEN)
Bakary Gassama (GAM)
Gehad Grisha (EGY)
Janny Sikazwe (ZAM)
Bamlak Tessema Weyesa (ETH)


NORD- och MELLAN-AMERIKA

Joel Aguilar (SLV)
Mark Geiger (USA)
Jair Marrufo (USA)
Ricardo Montero (CRC)
John Pitti (PAN)
César Arturo Ramos (MEX)


SYD-AMERIKA

Julio Bascuñán (CHI)
Enrique Cáseres (PAR)
Andrés Cunha (URU)
Néstor Pitana (ARG)
Sandro Ricci (BRA)
Wilmar Roldán (COL)


OCEANIEN

Matthew Conger (NZL)
Norbert Hauata (TAH)


EUROPA

Felix Brych (GER)
Cüneyt Çakir (TUR)
Sergei Karasev (RUS)
Björn Kuipers (NED)
Szymon Marziniak (POL)
Antonio Mateu Lahoz (ESP)
Milorad Mažić (SRB)
Gianluca Rocchi (ITA)
Damir Skomina (SVN)
Clément Turpin (FRA)
