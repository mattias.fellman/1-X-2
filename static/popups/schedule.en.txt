FIFA 2018 Groups (2 per group qualifies)

A
Egypt (EGY)
Saudi Arabia (KSA)
Uruguay (URU)
Russia (RUS)

B
Spain (ESP)
Iran (IRN)
Morocco (MAR)
Portugal (POR)

C
Australia (AUS)
Peru (PER)
France (FRA)
Denmark (DEN)

D
Argentina (ARG)
Iceland (ISL)
Croatia (CRO)
Nigeria (NGA)

E
Brazil (BRA)
Costa Rica (CRC)
Serbia (SRB)
Switzerland (SUI)

F
South Korea (KOR)
Mexico (MEX)
Sweden (SWE)
Germany (GER)

G
Belgium (BEL)
England (ENG)
Panama (PAN)
Tunisia (TUN)

H
Japan (JPN)
Colombia (COL)
Poland (POL)
Senegal (SEN)

ROUND OF 16

Match 49 1A-2B
Match 50 1C-2D
Match 51 1B-2A
Match 52 1D-2C
Match 53 1E-2F
Match 54 1G-2H
Match 55 1F-2E
Match 56 1H-2G


QUARTER-FINAL

Match 57 1A/2B-1C/2D
Match 58 1E/2F-1G/2H
Match 59 1B/2A-1D/2C
Match 60 1F/2E-1H/2G

SEMI-FINAL
Winners from games 57 and 58
Winners from games 59 and 60


MEDAL GAMES
Bronze game
Losers from semi
Final
Winners from semi
