$(document).ready(function(){
    $("#RoundOf16Div, #QuarterfinalDiv, #SemifinalDiv, #KnockoutDiv").click(function(event){
        elem = $(this).attr("data-elem");
        maxChecked = $(this).attr("data-maxChecked");
        CheckBoxUtil(elem, maxChecked);
    });

    $("#BetSlipForm").submit(function(event){
        event.preventDefault();
        // Validate check boxes
        liveNotChecked = $("#BetSlipForm").find('input:checkbox:enabled:not(:checked)').length;
        if (liveNotChecked > 0) {
            $("#ValModal").modal("show");
            return;
        }

        // Validate pseudonym again, a long time since last time...
        if (! IsPseudFree()) {
            return;
        }
        $("#SubmitBetSlipModal").modal("show");
    });
    
    $("#SignUpForm").submit(function(event){
        var pass = $("#password").val();
        var passConfirm = $("#password_confirm").val();
        if (pass != passConfirm) {
            event.preventDefault();
            $("#ErrorModal").modal("show");
            return false;
        }
    });

    $("#SendSlip").click(function(){
        $.ajax({
            url: $("#BetSlipForm").attr("action"),
            type: "POST",
            data: $("#BetSlipForm").serialize(),
            success: function(){
                $("#BetSlipForm").each(function(){
                    this.reset();
                });
                $("#iAgree").prop("checked", false); 
                $("#FillInContent").toggle();
                $("#BetSlipContent").toggle();
                $("#SubmitBetSlipModal").modal("hide");
                $("#SuccessModal").modal("show");  
            },
            error: function (xhr, httpStatusMessage, customErrorMessage) {
                $("#SubmitBetSlipModal").modal("hide");
                $("#ErrorModal").modal("show");
            },
        });
    });

    $("#AnotherBtn, #ViewBetsBtn").click(function(){
        redirectTo = $(this).attr("data-url");
        $(location).attr("href", redirectTo);
    });

    $("#FillInBtn").click(function(){
        agree = $("#iAgree").is(":checked") ? "true" : "false";
        if (agree == "false") {
            $('#FillInModal').modal('show');
        } else {
            $("#FillInContent").toggle();
            $("#BetSlipContent").toggle();
        }
    });

	$("#Pseud").blur(function(){
        if (IsPseudFree()) {
            var pseud = $("#Pseud").val();
            var str = $("#PWithPseud").html();
            var res = str.replace("[PSEUD]", pseud);
            $("#PWithPseud").html(res);
        }
    });

    $(".Approve, .Discard").click(function(){
        SetBetSlipStatus($(this).attr("data-slipId"), $(this).attr("data-newStatus"));
    });

    $(".HideBlogpost").click(function(){
        var id = $(this).attr("data-blogpostId");
        $.post("blogpost-hide", { BlogpostId: id })
        .done(function() {
            var cssClass = "table-danger";
            $("#tr"+id+" th, #tr"+id+" td").addClass(cssClass).fadeOut(1000).remove("#tr"+id);
        })
        .fail(function( data ) {
          alert( "Action failed, server returned: " + data );
        });
        return;
    });

    $("#ResetEvent").click(function(){
        $("#ResetForm").submit();
    });

    $("#ResultFormSubmit").click(function(){
        // Validate check boxes
        liveNotChecked = $("#ResultForm").find('input:checkbox:enabled:not(:checked)').length;
        if (liveNotChecked > 0) {
            $("#ValModal").modal("show");
            return;
        } else {
            $("#ResultForm").submit();
        }
    });
});

function IsPseudFree(pseud){
    var out = true;
    var pseud = $("#Pseud").val();
    var errMsg = $("#Pseud").attr("data-err");
    if (pseud.length < 2) {
        $("#Pseud").removeClass("is-valid").addClass("is-invalid");
        $("#PseudHelp").html(errMsg);
        return false;
    }
    $.post("pseudonyms",
        {
          Pseud: $("#Pseud").val()
        },
        function(data, status){
            data = $.parseJSON( data );
            if (data.Result == "true") {
                $("#Pseud").removeClass("is-invalid").addClass("is-valid");
                $("#PseudHelp").html(data.Message);
            } else {
                $("#Pseud").removeClass("is-valid").addClass("is-invalid");
                $("#PseudHelp").html(data.Message);
                $("#Pseud").focus();
                out = false;
            }
    });
    return out;
} 

function SetBetSlipStatus(id, st){
    $.post("set-betslip-status", { NewStatus: st, SlipId: id })
        .done(function() {
            if (st == "Approve") {
                var cssClass = "table-success";
            } else {
                var cssClass = "table-danger";
            }
            $("#tr"+id+" th, #tr"+id+" td").addClass(cssClass).fadeOut(1000).remove("#tr"+id);
        })
        .fail(function( data ) {
          alert( "Action failed, server returned: " + data );
        });
    return;
}

function CheckBoxUtil(elem, maxChecked) {
    checked = $("#"+elem+"Div").find('input:checkbox:checked').length;
    $("#"+elem+"Pill").html(checked);
    if (checked == maxChecked) {
        $("#"+elem+"Div").find('input:checkbox:not(:checked)').attr('disabled', 'disabled');
    }
    if (checked < maxChecked) {
        $("#"+elem+"Div").find('input:checkbox:not(:checked)').removeAttr('disabled');
    }
}